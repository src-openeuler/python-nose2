%global _empty_manifest_terminate_build 0
Name:		python-nose2
Version:	0.15.1
Release:	1
Summary:	Next generation of nicer testing for Python
License:	BSD-2-Clause
URL:		https://github.com/nose-devs/nose2
Source0:	https://github.com/nose-devs/nose2/archive/refs/tags/%{version}.tar.gz
BuildArch:	noarch

%description
nose2 is the next generation of nicer testing for Python, based on the plugins
branch of unittest2. nose2 aims to improve on nose by:
- providing a better plugin API
- being easier for users to configure
- simplifying internal interfaces and processes
- supporting Python 2 and 3 from the same codebase, without translation
- encouraging greater community involvement in its development

In service of some those goals, some features of nose will not be supported in
nose2. See the documentation for a thorough rundown.

%package -n python3-nose2
Summary:	Next generation of nicer testing for Python
Provides:	python-nose2 = %{version}-%{release}
BuildRequires:	python%{python3_pkgversion}-devel
BuildRequires:	python%{python3_pkgversion}-setuptools
BuildRequires:	python%{python3_pkgversion}-six
BuildRequires:	python%{python3_pkgversion}-mock
BuildRequires:	python%{python3_pkgversion}-cov-core
Requires:	python%{python3_pkgversion}-setuptools
Requires:	python%{python3_pkgversion}-six >= 1.6
Requires:	python%{python3_pkgversion}-cov-core >= 1.12

%description -n python3-nose2
nose2 is the next generation of nicer testing for Python, based on the plugins
branch of unittest2. nose2 aims to improve on nose by:
- providing a better plugin API
- being easier for users to configure
- simplifying internal interfaces and processes
- supporting Python 2 and 3 from the same codebase, without translation
- encouraging greater community involvement in its development

In service of some those goals, some features of nose will not be supported in
nose2. See the documentation for a thorough rundown.

%package help
Summary:	Development documents and examples for nose2
Provides:	python3-nose2-doc

%description help
nose2 is the next generation of nicer testing for Python, based on the plugins
branch of unittest2. nose2 aims to improve on nose by:
- providing a better plugin API
- being easier for users to configure
- simplifying internal interfaces and processes
- supporting Python 2 and 3 from the same codebase, without translation
- encouraging greater community involvement in its development

In service of some those goals, some features of nose will not be supported in
nose2. See the documentation for a thorough rundown.

%prep
%autosetup -n nose2-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-nose2 -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Aug 15 2024 xu_ping <707078654@qq.com> - 0.15.1-1
- Update package to version 0.15.1
- Fix a bug with config loading which caused custom ini configs 
   not to load if they were not named with a .cfg extension. 
- Official support for python3.13 betas. 

* Fri May 31 2024 wuzhaomin <wuzhaomin@kylinos.cn> - 0.14.1-1
- Update package to version 0.14.1
- Fix verbose reporting of skipped tests
- Fix testing-from-sdist CI build
- Fix typos (codespell)

* Fri May 05 2023 wubijie <wubijie@kylinos.cn> - 0.13.0-1
- Update package to version 0.13.0

* Tue Nov 08 2022 wangjunqi <wangjunqi@kylinos.cn> - 0.12.0-1
- Update package to version 0.12.0

* Wed Jun 29 2022 yaoxin <yaoxin30@h-partner.com> - 0.11.0-1
- Update to 0.11.0

* Tue May 10 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 0.9.2-2
- License compliance rectification

* Thu Jun 11 2020 Dillon Chen <dillon.chen@turbolinux.com.cn> - 0.9.2-1
- build for openEuler
